# ThirdPersonCamera_Playable
The playable version of the Third Person Camera project (have fun).
Code version so you can see the camera scripts may be coming along.

# Unity Version Number
2020.3.19f1


# Description
there are four Identical stages with the only difference being the camera controls, these being as follows:
Stage A: camera stays in a fixed position behind the player.
Stage B: camera moves with secondary player input (the mouse), creating a player controlled camera.
Stage C: camera moves with the player movement.
Stage D: camera moves with the player movement like stage C, but if the player holds down mouse button 1, it will follow the mouse like in stage B. 

This generates 4 text files named
 a_stageresults.txt
 b_stageresults.txt
 c_stageresults.txt
 d_stageresults.txt
 in
 C:\Users\<NAME>\AppData\LocalLow\DefaultCompany\3rdPersonCameraDisertation
 where you replace < NAME > with whatever your computer's username is.
 So yeah, check appdata for your times and falls.

# How to play
WASD to move, space to jump, mouse for camera movement.

# Installation
Download this repo and unzip the zip file.
The game should be ready-to-play as soon as the zip is downloaded.

# Usage
You can play the game, that's really all that's even possible, Good luck.

# Support
https://gitlab.com/Paige-K-afk
 https://github.com/Paige-K-afk
 If you can send messages on github or gitlabs be my guest.

# Contributing
No attempting to contribute please. It's a complete project. You will be rejected.

# Authors and acknowledgment
Author: Me (Paige) Acknowledements:
I'd like to thank my lecturer for the game behavior module at the University of Derby in 2022-2023 as he was also my dissertation supervisor.
